import { Component } from "@angular/core";

@Component({
	selector: 'wfm-loader',
	template: '<div class="loader-anomator"></div>',
	styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {
	
}