import { BaseApi } from './../core/base.api';
//import { User } from './../models/user.model';
import { Response } from '@angular/http';
import { Injectable, OnInit } from '@angular/core';
import { Observable, observable } from 'rxjs';
import { User } from '../models/user.model';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';


@Injectable()
export class UserService extends BaseApi{
	constructor(
		public http: HttpClient
	) {
		super(http);
	 }

	getUserByEmail(email: string): Observable<User> { //!
		return this.http.get<User[]>(`http://localhost:3000/users?email=${email}`)
			.pipe(
				map( (user: User[]) => user[0] ? user[0] : undefined)
				// map((users: User[]) => users[0] ? users[0] : undefined)
			);
	}
	
	// getUserByEmail(email: string): Observable<User> {
	// 	return this.get[](`users?email=${email}`)
	// }

	// createNewUser(user: User): Observable<any> { // ?
	// 	return this.http.post('http://localhost:3000/users', user)
		
	// }
	createNewUser(user: User): Observable<any> {
		return this.post('users', user)
		
	}


	// getUserByName(name: string): Observable<User[]> {
	// 	return this.http.get<User[]>(`http://localhost:3000/users?email=${name}`);
	// }
}

// export class MyPage implements OnInit {

	
// 	constructor(
// 		private userService: UserService
// 	){}
		
// 	ngOnInit(): void {
// 		this.userService.getUserByName('Vas')
// 			.pipe(
// 				map((data: User[]) => data.filter( item => item.name.length > 5)),
// 				map((data: User[]) => data.map( item => {
// 					item.name = item.name.toUpperCase();
// 					//....
// 					return item;
// 				}))
// 			).subscribe( (data: User[]) => {
// 				console.log(data);
				
// 			});
// 	}

// }