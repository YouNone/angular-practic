import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

import { Category } from '../../shared/models/category.model';
import { Message } from './../../../shared/models/message.model';
import { CategoriesService } from './../../shared/services/categories.service';

@Component({
	selector: 'wfm-edit-category',
	templateUrl: './edit-category.component.html',
	styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit, OnDestroy {

	sub1: Subscription;

	@Input() categories: Category[] = [];
	@Output() onCategoryEdit = new EventEmitter<Category>();

	currentCategoryId = 1;
	currentCategory: Category;
	message: Message;

	constructor(private categoriesServise: CategoriesService) { }

	ngOnInit() {
		this.message = new Message("success", "")
		this.onCategoryChange();
	}
	onCategoryChange() {
		// console.log(this.currentCategoryId);
		this.currentCategory = this.categories
			.find(c => c.id === +this.currentCategoryId);
		
	}
	onSubmit(form: NgForm){
		let {capacity, name} = form.value;

		if (capacity < 0) capacity *= -1;

		const category = new Category(name, capacity, +this.currentCategoryId)

		this.categoriesServise
			.updateCategory(category) 
			.subscribe((category: Category) => {
				this.onCategoryEdit.emit(category);
				this.message.text = "Категория успешно отредактирована.";
				window.setTimeout(() => this.message.text = "", 5000)
				// console.log(category);
		});
	}
	ngOnDestroy() {
		if (this.sub1) {
			this.sub1.unsubscribe();
		}
	}
}
