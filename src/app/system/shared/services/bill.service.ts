import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from "@angular/core";

import { Bill } from '../models/bill.model';
import { BaseApi } from './../../../shared/core/base.api';

@Injectable()
export class BillService extends BaseApi {

	constructor(public http: HttpClient) {
		super(http);
	}

	// getBill(): Observable<Bill> {
	// 	return this.http.get<Bill>('http://localhost:3000/bill');
	// }
	
	getBill(): Observable<Bill> {
		return this.get("bill");
	}

	updateBill(bill: Bill): Observable<Bill>{
		return this.put("bill", bill);
	}

	getCurrency(base: string = 'EUR'): Observable<any> {
		return this.http.get(`http://data.fixer.io/api/latest?base=${base}&access_key=72bceb5d9d733888eb96a1ef7f524d55&symbols=USD,EUR,RUB`);
	}
}