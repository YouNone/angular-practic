import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { BaseApi } from './../../../shared/core/base.api';
import { WfmEvent } from './../models/event.model';

@Injectable()

export class EventsService extends BaseApi {
	constructor(public http: HttpClient) {
		super(http);
	}

	addEvent(event: WfmEvent) :Observable<WfmEvent> {
		return this.post("events", event);
	}
	getEvents(): Observable<WfmEvent[]> {
		return this.get('events');
	}
	getEventsById(id: string) : Observable<WfmEvent> {
		return this.get(`events/${id}`);
	}
}