import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { CategoriesService } from '../../shared/services/categories.service';
import { EventsService } from '../../shared/services/events.service';
import { WfmEvent } from '../../shared/models/event.model';
import { Category } from '../../shared/models/category.model';

@Component({
	selector: 'wfm-history-detail',
	templateUrl: './history-detail.component.html',
	styleUrls: ['./history-detail.component.scss']
})
export class HistoryDetailComponent implements OnInit, OnDestroy {
	
	event: WfmEvent;
	category: Category;

	isLoaded = false;

	s1: Subscription;


	constructor(
		private route: ActivatedRoute,
		private eventService: EventsService,
		private categoriesService: CategoriesService

	) { }

	ngOnInit() {
		this.s1 = this.route.params
			.pipe(
				mergeMap((params: Params) => this.eventService.getEventsById(params["id"])),
				mergeMap((event: WfmEvent) => {
					this.event = event;
					return this.categoriesService.getCategoryId(event.category);
				})
			)
			.subscribe((category: Category) => {
				this.category = category;
				
				this.isLoaded = true;
			});
	}
	ngOnDestroy() {
		if (this.s1) {
			this.s1.unsubscribe();
		}
	}
}
