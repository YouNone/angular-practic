import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription, combineLatest } from 'rxjs';
import * as moment from 'moment';

import { CategoriesService } from './../shared/services/categories.service';
import { WfmEvent } from './../shared/models/event.model';
import { EventsService } from './../shared/services/events.service';
import { Category } from '../shared/models/category.model';

@Component({
	selector: 'wfm-history-page',
	templateUrl: './history-page.component.html',
	styleUrls: ['./history-page.component.scss']
})
export class HistoryPageComponent implements OnInit, OnDestroy {
	sub1: Subscription;

	constructor(
		private categoriesServise: CategoriesService,
		private eventService: EventsService,
	) { }

	isLoaded = false;

	categories: Category[] = [];
	events: WfmEvent[] = [];
	filteredEvents: WfmEvent[] = [];

	chartData = [];

	isFilterVisable = false;

	ngOnInit() {
		this.sub1 = combineLatest(
			this.categoriesServise.getCategories(),
			this.eventService.getEvents()
		).subscribe((data: [Category[], WfmEvent[]]) => {
			this.categories = data[0];
			this.events = data[1];

			this.setOriginalEvents();

			this.calculateChartData();

			this.isLoaded = true;
		});
	}

	private setOriginalEvents() {
		this.filteredEvents = this.events.slice();
	}

	calculateChartData() :void {
		this.chartData = [];
		this.categories.forEach((cat) => {
			const catEvent = this.filteredEvents.filter((e) => e.category === cat.id && e.type === "outcome");

			this.chartData.push({
				name: cat.name,
				value: catEvent.reduce((total, e) => {
					total += e.amount;
					return total;
				}, 0)
			});
		});
	}
	ngOnDestroy() {
		if (this.sub1) {
			this.sub1.unsubscribe();
		}
	}

	private toggleFilterVisability(dir: boolean) {
		this.isFilterVisable = dir;
	}

	openFilter() {
		this.toggleFilterVisability(true);
	}

	onFilterApply(filterData) {
		this.toggleFilterVisability(false);
		this.setOriginalEvents();

		const startPeriod = moment().startOf(filterData.period).startOf('d');
		const endPeriod = moment().endOf(filterData.period).endOf('d');
		
		console.log(filterData);

		this.filteredEvents = this.filteredEvents
			.filter((e) => {
				return filterData.types.indexOf(e.type) !== -1;
			})
			.filter((e) => {
				return  filterData.categories.indexOf(e.category.toString()) !== -1;
			})
			.filter((e) => {
				const momentDate = moment(e.date, 'DD.MM.YYYY HH:mm:ss');
				return momentDate.isBetween(startPeriod, endPeriod);
			});
		this.calculateChartData();
	}

	onFilterCancel() {
		this.toggleFilterVisability(false);
		this.setOriginalEvents();
		this.calculateChartData();
	}
}
