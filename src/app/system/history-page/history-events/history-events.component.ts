import { Component, OnInit, Input } from '@angular/core';

import { Category } from './../../shared/models/category.model';
import { WfmEvent } from '../../shared/models/event.model';

@Component({
	selector: 'wfm-history-events',
	templateUrl: './history-events.component.html',
	styleUrls: ['./history-events.component.scss']
})
export class HistoryEventsComponent implements OnInit {
	@Input() categories: Category[] = [];
	@Input() events: WfmEvent[] = [];

	search = {
		value: '',
		placeholder:'Сумма',
		field: 'amount'
	}
	

	constructor() { }

	ngOnInit() {
		this.events.forEach((e) => {
			e.catName = this.categories.find(c => c.id === e.category).name;
		})
	}
	getEventClass(e: WfmEvent){
		return {
			'label': true,
			'label-danger': e.type === 'outcome',
			'label-success': e.type === 'income'

		}
	}
	changeCriteria(field: string) {
		const namesMap = {
			amount: "Сумма",
			date: "Дата",
			category: "Категория",
			type: "Тип"
		};
		this.search.placeholder = namesMap[field];
		this.search.field = field;
	}

}
