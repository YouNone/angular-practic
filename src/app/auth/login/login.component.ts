import { Component, OnInit, HostBinding } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { fadeStateTrigger } from './../../shared/animations/fade.animatin';
import { AuthService } from './../../shared/services/auth.service';
import { User } from '../../shared/models/user.model';
import { UserService } from '../../shared/services/user.service';
import { Message } from '../../shared/models/message.model';
import { Title, Meta } from '@angular/platform-browser';


@Component({
	selector: 'wfm-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
	animations: [fadeStateTrigger]
})
export class LoginComponent implements OnInit {

	form: FormGroup;
	message: Message;

		constructor(
		private userService: UserService,
		private authService: AuthService,
		private router: Router,
		private route: ActivatedRoute,
		private title: Title,
		private meta: Meta
	){
		title.setTitle('Вход в ситему');
		meta.addTags([
			{ name: 'keywords', content: 'логин, вход, система'},
			{ name: 'description', content: 'Страница для входа в систему'}
		]);
	}
		
	ngOnInit() {
		this.message = new Message('danger', '')
		this.route.queryParams
			.subscribe((params: Params) => {
				
				if (params['nowCanLogin']) {
					this.showMessage({
						text: 'Теперь вы можете войти в систему',
						type: 'success'
					});
				} else if(params['accessDenied']) {
					this.showMessage({
						text: 'Для работы с системой вам нужно залогиниться',
						type: 'warning'
					});
				}
			});

	
		this.form = new FormGroup({
			'email': new FormControl(null, [Validators.required, Validators.email]),
			'password': new FormControl(null, [Validators.required, Validators.minLength(6)])
		});
	}
	private showMessage(message: Message) {
		this.message = message;
		// setTimeout( function() {
		// 	this.message.text = ''; 
		// }, 5000);
	}
	onSubmit() {
		const formData = this.form.value;
		this.userService.getUserByEmail(formData.email)
		.subscribe((user: User) => {
			if (user) {
				if (user.password === formData.password) {
					this.message.text = '';
					window.localStorage.setItem('user',JSON.stringify(user));
					this.authService.login();
					this.router.navigate(['/system', 'bill']);
				} else {
					this.showMessage({
						text: "wrong password",
						type: 'danger'
					})
				}
			} else {
				this.showMessage({
					text: "no user exist",
					type: 'danger'
				})
			}
			
		})
	}

}
// ngOnInit(): void {
	// 	this.userService.getUserByName('Vas')
	// 		.pipe(
	// 			map((data: User[]) => data.filter( item => item.name.length > 5)),
	// 			map((data: User[]) => data.map( item => {
	// 				item.name = item.name.toUpperCase();
	// 				//....
	// 				return item;
	// 			}))
	// 		).subscribe( (data: User[]) => {
	// 			console.log(data);
				
	// 		});
	// }